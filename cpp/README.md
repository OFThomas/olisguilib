# Olis GUI lib

## REQUIRES olistdlib
    https://gitlab.com/OFThomas/olistd

## Ubuntu dependencies
    sudo apt-get update
    sudo apt-get install cmake make g++ libx11-dev libxi-dev libgl1-mesa-dev libglu1-mesa-dev -y
    sudo apt-get install libxrandr-dev libxext-dev libxcursor-dev libxinerama-dev libxi-dev -y
    sudo apt-get install libglew-dev libglfw3-dev libglm-dev -y

