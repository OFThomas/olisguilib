#version 330 core
layout (location = 0) in vec3 pos;

out vec4 vertexcolour;

void main()
{
    gl_Position = vec4(pos, 1.0);
    vertexcolour = vec4(0.5, 0.0, 0.0, 1.0);
}
