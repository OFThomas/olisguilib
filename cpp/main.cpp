// Include standard headers
#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include <array>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "opengl.hpp"

#include <olistd/io>
#include <olistd/random>

class Coord
{
    private:
    public:
        float x = 0.0;
        float y = 0.0;
        float z = 0.0;

        Coord(float x, float y) : x(x), y(y) {}
        Coord(float x, float y, float z) : x(x), y(y), z(z) {}
        Coord(const std::vector<float> & c) : x(c[0]), y(c[1]), z(c[2]) {}
        Coord(const std::array<float, 3> & c) : x(c[0]), y(c[1]), z(c[2]) {}

        operator std::array<float, 3>() const
        {
            return {x, y, z};
        }
};

class RGB
{
    private:
    public:
        float r = 1.0;
        float g = 1.0;
        float b = 1.0;

        RGB(float r = 1.0, float g = 1.0, float b = 1.0) : r(r), g(g), b(b) {}
        RGB(const std::vector<float> & c) : r(c[0]), g(c[1]), b(c[2]) {}
        RGB(const std::array<float, 3> & c) : r(c[0]), g(c[1]), b(c[2]) {}

        operator std::array<float, 3>() const
        {
            return {r, g, b};
        }
};

class Point 
{
    private:
    public:
        Coord coord;
        RGB rgb;

        Point(const Coord & c) : coord(c) {}
        Point(const Coord & c, const RGB & col) : coord(c), rgb(col) {}

        operator std::array<float, 6>() const 
        {
            return {coord.x, coord.y, coord.z, rgb.r, rgb.g, rgb.b};
        }

        operator std::vector<float>() const 
        {
            return {coord.x, coord.y, coord.z, rgb.r, rgb.g, rgb.b};
        }

        float sq_dist(const Point & p) const
        {
            return olistd::sq(coord.x - p.coord.x) 
                + olistd::sq(coord.y - p.coord.y) 
                + olistd::sq(coord.z - p.coord.z);
        }
};


class Triangle
{
    private:
    public:
        std::vector<Point> points;

        Triangle(const std::vector<Point> & points)
        {
        }
        Triangle(const Point & p1, const Point & p2, const Point & p3) : Triangle(std::vector<Point>{p1, p2, p3}) {}
};

std::array<float, 18> make_triangle(const std::array<Point, 3> & p)
{
    std::array<float, 18> v;

    for(size_t i = 0; i < 3; i++)
    {
        // const Point & point = p[i];
        const std::array<float, 6> & point = p[i];
        for(size_t j = 0; j < 6; j++)
        {
            v[i * 6 + j] = point[j];
        }
    }
    return v;
}

std::array<float, 18> make_triangle(const Point & p1, const Point & p2, const Point & p3)
{
    return make_triangle(std::array<Point, 3>{p1, p2, p3});
}

std::vector<float> make_triangle2d(float x1, float y1, float x2, float y2, float x3, float y3)
{
    std::vector<float> v =
    {
        x1, y1, 0.0f, 
        x2, y2, 0.0f,
        x3, y3, 0.0f 
    };

    return v;
}

std::vector<float> square(float x1, float y1, float x2, float y2)
{
    std::vector<float> v1 = make_triangle2d( x1, y1, x1, y2, x2, y1);
    std::vector<float> v2 = make_triangle2d( x2, y2, x2, y1, x1, y2);
    v1.insert(v1.end(), v2.begin(), v2.end());
    return v1;
}
class Shape
{
    private:

        std::vector<Point> make_triangles(const std::vector<Point> & points)
        {
            if(points.size() % 3 == 0)
                return points;

            if(points.size() < 3)
            {
                std::cerr << "ERR less than 3 points in your shape!" << std::endl;
                return points;
            }
            else 
            {
                std::vector<Point> v(points.begin(), points.end());
                // look at the last point 
                const Point & lastp = points[points.size()-1];

                // need to find the nearest two points 
                float dist1 = 1e99;
                float dist2 = 1e99;

                size_t idx1 = 0;
                size_t idx2 = 0;

                for(size_t i = 0; i < points.size() - 1; i++)
                {
                    const Point & cur_p = points[i];
                    float dist = lastp.sq_dist(cur_p);
                    if(dist < dist1)
                    {
                        dist2 = dist1;
                        idx2 = idx1;
                        dist1 = dist;
                        idx1 = i;
                    }
                    else if(dist < dist2)
                    {
                        dist2 = dist;
                        idx2 = i;
                    }
                }

                if(points.size() % 3 == 1) // square
                {
                    v.push_back(points[idx1]);
                    v.push_back(points[idx2]);
                }
                else if(points.size() % 3 == 2) // idk but only add one extra point to make tri
                {
                    v.push_back(points[idx1]);
                }

                return v;
            }

        }

    public:

        std::vector<Point> points;

        Shape(const std::vector<Point> & points_in) : points(make_triangles(points_in))
    {}

        operator std::vector<float>() const 
        {
            std::vector<float> v;
            v.reserve(points.size() * 6);
            for(const Point & p : points)
            {
                std::vector<float> pv = p;
                v.insert(v.end(), pv.begin(), pv.end());
            }
            return v;
        }

        void project(float delta_z = 0.5f)
        {

            // for each point 
            //  a1 -- b2    a2 -- b2
            //  |   /        |   /
            //  | /          | / 
            //  c1          c2

            // then rectangles between pairs of points in the different faces
            // a1, a2, c1, c2
            // a1, a2, b1, b2
            // b1, b2, c1, c2

            std::vector<Point> bot_face{points};
            for(Point & p : bot_face)
                p.coord.z += delta_z;

            const std::vector<Point> face{points};
            for(size_t i = 0; i < face.size(); i++)
            {
                size_t idx = i;
                size_t idxp = (i+1)%face.size();
                std::vector<Point> side = {face[idx], bot_face[idx], face[idxp], 
                    face[idxp], bot_face[idxp],bot_face[idx] };
                for(Point & p : side)
                {
                    p.rgb.r -= 0.2f * (i+1);
                    p.rgb.b -= 0.1f * (i+1);
                    p.rgb.g -= 0.15f * (i+1);
                }
                points.insert(points.end(), side.begin(), side.end());
            }
            points.insert(points.end(), bot_face.begin(), bot_face.end());
        }
};

class Rect : public Shape
{
    private:
    public:

        Rect(const Point & p1, const Point & p2, const Point & p3, const Point & p4) : Shape({p1,p2,p3, p3, p4, p1})
        {}

        void project(float delta_z = 0.5f)
        {

            // for each point 
            //  a1 -- b2    a2 -- b2
            //  |   /        |   /
            //  | /          | / 
            //  c1          c2

            // then rectangles between pairs of points in the different faces
            // a1, a2, c1, c2
            // a1, a2, b1, b2
            // b1, b2, c1, c2

            std::vector<Point> bot_face{points};
            for(Point & p : bot_face)
                p.coord.z += delta_z;

            const std::vector<Point> face{points};

            std::vector<size_t> indices = {0,1,2,4};
            for(size_t i = 0; i < indices.size(); i++)
            {
                size_t idx = indices[i];
                size_t idxp = indices[(i+1) % indices.size()];

                std::vector<Point> side = {face[idx], bot_face[idx], face[idxp], 
                    face[idxp], bot_face[idxp], bot_face[idx] };

                // Rect side{face[idx], face[idxp], bot_face[idx], bot_face[idxp]};

                for(Point & p : side)
                {
                    p.rgb.r -= 0.2f * (i);
                    p.rgb.b -= 0.1f * (i);
                    p.rgb.g -= 0.15f * (i);
                }
                points.insert(points.end(), side.begin(), side.end());
            }
            points.insert(points.end(), bot_face.begin(), bot_face.end());

            std::cout << points.size() << std::endl;
        }

};

glm::vec3 random_position(void)
{
    return glm::vec3(olistd::random_number<float>(-4.0f, 4.0f), 
            olistd::random_number<float>(-4.0f, 4.0f), 
            olistd::random_number<float>(-15.0f, 0.0f));
}


int main( void )
{

    Window window;

    glfwMakeContextCurrent(window);

    // tell GLFW to capture our mouse
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glEnable(GL_DEPTH_TEST);

    // Create and compile our GLSL program from the shaders
    const std::string vertex_path = "SimpleVertexShader.vertexshader";
    const std::string fragment_path = "SimpleFragmentShader.fragmentshader";
    // Shaders shaders{"../", vertex_path, fragment_path};

    const char *vertexShaderSource ="#version 330 core\n"
        "layout (location = 0) in vec3 aPos;\n"
        "layout (location = 1) in vec3 aColor;\n"
        "out vec3 ourColor;\n"
        "out vec3 vpos;\n"
        "uniform mat4 model;\n"
        "uniform mat4 view;\n"
        "uniform mat4 projection;\n"
        "void main()\n"
        "{\n"
        "   gl_Position = projection * view * model * vec4(aPos, 1.0f);\n"
        "   vpos = aPos;\n"
        "   ourColor = aColor;\n"
        "}\0";

//     const char *fragmentShaderSource = "#version 330 core\n"
//         "out vec4 FragColor;\n"
//         "in vec3 ourColor;\n"
//         "in vec3 vpos;\n"
//         "uniform float u_time;\n"
//         "void main()\n"
//         "{\n"
//         "   FragColor = vec4(ourColor.x + sin(u_time), ourColor.y, ourColor.z, 1.0f);\n"
//         "}\n\0";

const char *fragmentShaderSource = "#version 330 core\n"
        "out vec4 FragColor;\n"
        "in vec3 ourColor;\n"
        "in vec3 vpos;\n"
        "uniform float u_time;\n"
        "uniform vec2 u_resolution;\n"
        "void main()\n"
        "{\n"
        "   vec2 st = gl_FragCoord.xy / u_resolution;\n"
        "   float intens = 1.0f - abs(sin(5.0 * (0.5 * u_time - 4 * st.x)));\n"
        // "if(intens < 0.2)\n"
        // "   gl_FragColor = vec4(0.2, 0.2, intens, 1.0f);\n"
        // "else\n"
       "   gl_FragColor = vec4(0.8*ourColor.x - intens, 0.8*ourColor.y + intens, 0.8*ourColor.z + intens, 1.0f);\n"
        "}\n\0";



    Shaders shaders{vertexShaderSource, fragmentShaderSource};

    Vertexarray VAO;
    Buffer vbo;

    // Shape mytriangle{{
    //         Coord{0.0, 0.0}, 
    //             Coord{0.0, 0.5}, 
    //             Coord{0.5, 0.0}
    //     }};

    Rect rectangle{
        Coord{0.0, 0.0}, 
            Coord{1.0, 0.0},
            Coord{1.0,0.25},
            Coord{0.0, 0.25}, 
    };

    rectangle.project();
    vbo << rectangle;

    // olistd::print(std::vector<float>{rectangle});
    // std::vector<float> rv{rectangle};
    // for(size_t i = 0; i < rv.size(); i+=6)
    //     std::cout << rv[i] << ", " << rv[i+1] << ", " << rv[i+2] << std::endl;

    VAO.set_packed_colours();

    const float SCR_WIDTH = 800.0f;
    const float SCR_HEIGHT = 600.0f;
    const float aspect_ratio = SCR_WIDTH / SCR_HEIGHT;
    std::vector<glm::vec3> positions(10);
    for(glm::vec3 & p : positions)
        p = random_position();

    const float height_scale = 0.8f;
    for(size_t i = 0; i < positions.size(); i++)
        positions[i] = glm::vec3(0.0f, height_scale * (float)i -5.0f, -10.0f);


    while(not glfwWindowShouldClose(window))
    {
        // glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
        window.update_time();
        window.processInput();

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

        // Use our shader
        shaders.use();

        // create transformations
        // calculate the model matrix for each object and pass it to shader before drawing
        glm::mat4 projection = glm::perspective(glm::radians(window.fov), window.aspect_ratio, 0.1f, 100.0f);
        shaders.set("projection", projection);

        // glm::mat4 view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));
        glm::mat4 view = glm::lookAt(window.cam_pos, window.cam_pos + window.cam_front, window.cam_up);
        shaders.set("view", view);

        for(size_t col = 0; col < 5; col++)
        {
        for(size_t i = 0; i < positions.size(); i++)
        {

            glm::mat4 model = glm::mat4(1.0f);
            model = glm::translate(model, positions[i]);
        

            glm::vec3 colshift{(float)col * 1.72f, 0.0f, 0.0f};
            model = glm::translate(model, colshift);

            // float angle = 20.0f * i;
            float utime = (float)glfwGetTime();
            // float angle = utime * i * 10;
            shaders.set("model", model);
            shaders.set("u_time", utime);
            shaders.set("u_resolution", glm::vec2(SCR_WIDTH, SCR_HEIGHT));
            // Draw the triangle !
            glDrawArrays(GL_TRIANGLES, 0, 3 * vbo.num_shapes()); // 3 indices starting at 0 -> 1 triangle


            float angle = 45.0f;
            glm::vec3 rightshift{1.0f, 0.0f, 0.0f};
            glm::vec3 scaleshift{1.25f, 1.0f, 1.0f};
            
           if( (i+col) % 2 != 0)
            {
            rightshift = {0.83f, 0.078f, 0.0f};
            angle = -45.0f;
            }
            
           if(col % 2 != 0)
           {
           if(i == 0 || i == positions.size() - 1)
            {
                angle = 0.0f;
                scaleshift = {1.0f, 1.0f, 1.0f};
                // rightshift = {0.0f, 0.0f, 0.0f};
            }
           }

            model = glm::translate(model, rightshift);
            model = glm::rotate(model, glm::radians(angle), glm::vec3(0.0f, 0.0f, 1.0f));
            model = glm::scale(model, scaleshift);

            shaders.set("model", model);

            glDrawArrays(GL_TRIANGLES, 0, 3 * vbo.num_shapes()); // 3 indices starting at 0 -> 1 triangle

        }
        }
        // Swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();

    } 

    return 0;
}


