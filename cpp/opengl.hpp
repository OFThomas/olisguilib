/**
 * @author      : oli (oli@oli-desktop)
 * @file        : opengl
 * @created     : Thursday Nov 10, 2022 20:38:11 GMT
 */

#ifndef OPENGL_HPP
#define OPENGL_HPP

class Window
{
    private:
    public:
        GLFWwindow * window;
        operator GLFWwindow*() const { return window; }

        float lastframetime = 0.0f;
        float deltatime = 0.0f;

        glm::vec3 cam_pos = glm::vec3(0.0f, 0.0f, 3.0f);
        glm::vec3 cam_front = glm::vec3(0.0f, 0.0f, -1.0f);
        glm::vec3 cam_up = glm::vec3(0.0f, 1.0f, 0.0f);

        float fov = 45.0f;
        float aspect_ratio = 800.0f / 600.0f;

        float yaw = -90.0f;
        float pitch =  0.0f;
        float lastX =  800.0f / 2.0;
        float lastY =  600.0 / 2.0;
        bool firstMouse = true;

        Window()
        {
            // Initialise GLFW
            if( not glfwInit() )
            {
                fprintf( stderr, "Failed to initialize GLFW\n" );
                getchar();
                throw "Err initialize glfw";
            }

            glfwWindowHint(GLFW_SAMPLES, 4);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
            glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
            glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

            // Open a window and create its OpenGL context
            window = glfwCreateWindow( 1024, 768, "Tutorial 01", NULL, NULL);
            if( window == NULL )
            {
                fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
                getchar();
                glfwTerminate();
                throw "Err in opening GLFW window";
            }

            // see https://stackoverflow.com/questions/7676971/pointing-to-a-function-that-is-a-class-member-glfw-setkeycallback
            glfwMakeContextCurrent(window);
            glfwSetWindowUserPointer(window, this);

            auto f = [](GLFWwindow * w, int a, int b)
            {
                static_cast<Window *>(glfwGetWindowUserPointer(w))->framebuffer_size_callback(a,b);
            };
            glfwSetFramebufferSizeCallback(window, f);
            
            auto mf = [](GLFWwindow * w, double a, double b)
            {
                static_cast<Window *>(glfwGetWindowUserPointer(w))->mouse_callback(a,b);
            };
            glfwSetCursorPosCallback(window, mf);

            auto scrollf = [](GLFWwindow * w, double a, double b)
            {
                static_cast<Window *>(glfwGetWindowUserPointer(w))->scroll_callback(a,b);
            };
            glfwSetScrollCallback(window, scrollf);

            // Initialize GLEW
            if (glewInit() != GLEW_OK) 
            {
                fprintf(stderr, "Failed to initialize GLEW\n");
                getchar();
                glfwTerminate();
                throw "Err in initializing GLEW";
            }

            // Ensure we can capture the escape key being pressed below
            glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

            // Dark blue background
            glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
        }

        ~Window()
        {
            // Close OpenGL window and terminate GLFW
            glfwTerminate();
        }

        void update_time()
        {
            float currentframetime = static_cast<float>(glfwGetTime());
            deltatime = currentframetime - lastframetime;
            lastframetime = currentframetime;
        }

        // process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
        // ---------------------------------------------------------------------------------------------------------
        void processInput()
        {
            if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
                glfwSetWindowShouldClose(window, true);

            float cam_speed = static_cast<float>(2.5 * deltatime);
            if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
                cam_pos += cam_speed * cam_front;
            if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
                cam_pos -= cam_speed * cam_front;
            if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
                cam_pos -= glm::normalize(glm::cross(cam_front, cam_up)) * cam_speed;
            if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
                cam_pos += glm::normalize(glm::cross(cam_front, cam_up)) * cam_speed;
        }

        // glfw: whenever the window size changed (by OS or user resize) this callback function executes
        // ---------------------------------------------------------------------------------------------
        void framebuffer_size_callback(int width, int height)
        {
            // make sure the viewport matches the new window dimensions; note that width and 
            // height will be significantly larger than specified on retina displays.
            glViewport(0, 0, width, height);
        }

        // glfw: whenever the mouse moves, this callback is called
        // -------------------------------------------------------
        void mouse_callback(double xposIn, double yposIn)
        {
            float xpos = static_cast<float>(xposIn);
            float ypos = static_cast<float>(yposIn);

            lastX = 800.0f / 2.0f;
            lastY = 600.0f / 2.0f;

            yaw = -90.0f;
            pitch = 0.0f;

            cam_front = glm::vec3(0.0f, 1.0f, 0.0f);

            if(firstMouse)
            {
                lastX = xpos;
                lastY = ypos;
                firstMouse = false;
            }

            float xoffset = xpos - lastX;
            float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top
            lastX = xpos;
            lastY = ypos;

            float sensitivity = 0.05f; // change this value to your liking
            xoffset *= sensitivity;
            yoffset *= sensitivity;

            yaw += xoffset;
            pitch += yoffset;

            // make sure that when pitch is out of bounds, screen doesn't get flipped
            if (pitch > 89.0f)
                pitch = 89.0f;
            if (pitch < -89.0f)
                pitch = -89.0f;

            glm::vec3 front;
            front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
            front.y = sin(glm::radians(pitch));
            front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
            cam_front = glm::normalize(front);
        }

        // glfw: whenever the mouse scroll wheel scrolls, this callback is called
        // ----------------------------------------------------------------------
        void scroll_callback(double xoffset, double yoffset)
        {
            fov -= (float)yoffset;
            if (fov < 1.0f)
                fov = 1.0f;
            if (fov > 45.0f)
                fov = 45.0f;
        }


};

class Vertexarray
{
    private:
    public:

        GLuint ID;
        operator GLuint() const { return ID; }

        Vertexarray()
        {
            glGenVertexArrays(1, &ID);
            glBindVertexArray(ID);
        }

        ~Vertexarray()
        {
            glDeleteVertexArrays(1, &ID);
        }

        void set_packed_colours(void)
        {
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
            glEnableVertexAttribArray(0);

            // colour attribute buffer : 
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
            glEnableVertexAttribArray(1);

        }


};

class Buffer
{
    private:
    public:
        GLuint buffer;
        operator GLuint() const { return buffer; }

        std::vector<float> data;

        Buffer()
        {
            glGenBuffers(1, &buffer);
        }

        Buffer(const std::vector<float> & data_in) : data(data_in)
    {
        glGenBuffers(1, &buffer);
        bind();
    }

        Buffer(const std::initializer_list<float> & data_in) : data(std::vector<float>{data_in}) {}

        void bind()
        {
            glBindBuffer(GL_ARRAY_BUFFER, buffer);
            glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(data[0]), data.data(), GL_STATIC_DRAW);
        }

        ~Buffer()
        {
            glDeleteBuffers(1, &buffer);
        }

        void merge(const std::vector<float> & b1)
        {
            data.insert(data.end(), b1.begin(), b1.end());
            bind();
        }

        friend void operator<<(Buffer & buf, const std::vector<float> & datain)
        {
            buf.merge(datain);
        }

        std::size_t num_shapes() const 
        {
            return data.size() / 6;
        }
};

class Shaders
{
    private:
        std::string vpath;
        std::string fpath;

        std::string svcode;
        std::string sfcode;

        const char * vshader_src;
        const char * fshader_src;

        GLuint vertex;
        GLuint fragment;

        void load(const std::string & vpath, const std::string & fpath)
        {

            std::ifstream vfile;
            vfile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

            std::ifstream ffile;
            ffile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

            try
            {
                vfile.open(vpath);
                std::stringstream vstream;
                vstream << vfile.rdbuf();
                vfile.close();
                svcode = vstream.str();
                vshader_src = svcode.c_str();

                ffile.open(fpath);
                std::stringstream fstream;
                fstream << ffile.rdbuf();
                ffile.close();
                sfcode = fstream.str();
                fshader_src = sfcode.c_str();
            }
            catch(std::ifstream::failure e)
            {
                std::cerr << "ERR shaders not read" << std::endl;
            }
        }


        void compile()
        {
            compile(vshader_src, fshader_src);
        }

        void compile(const char * vcode, const char * fcode)
        {
            vertex = glCreateShader(GL_VERTEX_SHADER);
            // const char * vcode = svcode.c_str();
            glShaderSource(vertex, 1, &vcode, NULL);
            glCompileShader(vertex);

            {
                // print compile errors if any
                int success;
                char infoLog[512];
                glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
                if(!success)
                {
                    glGetShaderInfoLog(vertex, 512, NULL, infoLog);
                    std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << vpath 
                        << "\n" << infoLog << std::endl;
                    std::cout << vcode << std::endl;
                } 
            }

            fragment = glCreateShader(GL_FRAGMENT_SHADER);
            // const char * fcode = sfcode.c_str();
            glShaderSource(fragment, 1, &fcode, NULL);
            glCompileShader(fragment);

            {
                // print compile errors if any
                int success;
                char infoLog[512];
                glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
                if(!success)
                {
                    glGetShaderInfoLog(fragment, 512, NULL, infoLog);
                    std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << fpath 
                        << "\n" << infoLog << std::endl;
                }
            }
        }

        void init()
        {
            programid = glCreateProgram();
            glAttachShader(programid, vertex);
            glAttachShader(programid, fragment);
            glLinkProgram(programid);

            int success;
            glGetProgramiv(programid, GL_LINK_STATUS, &success);
            if(!success)
            {
                char infoLog[512];
                glGetProgramInfoLog(programid, 1024, NULL, infoLog);
                std::cout << "ERROR::PROGRAM_LINKING_ERROR of program: \n" 
                    << infoLog << "\n -- --------------------------------------------------- -- " 
                    << std::endl;
            }

            glDeleteShader(vertex);
            glDeleteShader(fragment);
        }


    public:

        GLuint programid;

        operator GLuint() const { return programid; }

        Shaders(const char * vsrc, const char * fsrc)
        {
            // std::cout << "const char * ctor shaders" << std::endl;
            compile(vsrc, fsrc);
            init();
        }


        Shaders(const std::string & vertex_path, const std::string & fragment_path) 
            : vpath(vertex_path), fpath(fragment_path)
        {
            load(vertex_path, fragment_path);
            compile();
            init();
        }

        Shaders(const std::string & path, const std::string & vertex_path, 
                const std::string & fragment_path) : Shaders(path+vertex_path, path+fragment_path) {}

        ~Shaders()
        {
            glDeleteProgram(programid);
        }

        void use()
        {
            glUseProgram(programid);
        }

        void set(const std::string & name, const glm::mat4 &mat) const
        {
            glUniformMatrix4fv(glGetUniformLocation(programid, name.c_str()), 1, GL_FALSE, &mat[0][0]);
        }
        void set(const std::string & name, const glm::vec2 & v) const
        {
            glUniform2fv(glGetUniformLocation(programid, name.c_str()), 1, &v[0]);

        }

        void set(const std::string & name, const float val) const
        {
            glUniform1f(glGetUniformLocation(programid, name.c_str()), val);
        }
};


#endif // end of include guard OPENGL_HPP 

